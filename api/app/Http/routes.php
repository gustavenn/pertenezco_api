<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});
	



	
$app->post('ws/puntosObtenerDatosUsuariosParam','WServiceController@puntosObtenerDatosUsuariosPostParam');

$app->get('ws/puntosObtenerDatosUsuarios','WServiceController@puntosObtenerDatosUsuarios');

$app->get('ws/puntosConsultaArticuloPuntos','WServiceController@puntosConsultaArticuloPuntos');

$app->get('ws/puntosConsultaDisponibles','WServiceController@puntosConsultaDisponibles');

$app->get('ws/puntosConsultaRubrosOpcion','WServiceController@puntosConsultaRubrosOpcion');

$app->get('ws/puntosConsultaValorPuntos','WServiceController@puntosConsultaValorPuntos');

$app->post('ws/puntosConsumir','WServiceController@puntosConsumir');

$app->get('ws/puntosDetalleExcepcion','WServiceController@puntosDetalleExcepcion');

$app->get('ws/puntosObtenerRubros','WServiceController@puntosObtenerRubros');








// Errors...
Log::error(404);
Log::error(503);