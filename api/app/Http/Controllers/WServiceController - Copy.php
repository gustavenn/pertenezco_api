<?php

namespace App\Http\Controllers;
 
use App\WService;
use Illuminate\Http\Request;
use Validator;
use Artisaninweb\SoapWrapper\Facades\SoapWrapper;


class WServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index()
    {


/*
$client = new SoapClient("http://path.to/wsdl?WSDL");
$res = $client->SoapFunction(array('param1'=>'value','param2'=>'value'));
echo $res->PaymentNotification->payment;

$soap_url = 'http://path/wsdl/somefile.wsdl';
   $soap_client = new SoapClient($soap_url);

   var_dump($soap_client->__getFunctions());
*/


                // Add a new service to the wrapper
        SoapWrapper::add(function ($service) {
            $service
                ->name('currency')
                ->wsdl('http://currencyconverter.kowabunga.net/converter.asmx?WSDL')
                ->trace(true)                                                   // Optional: (parameter: true/false)
              //  ->header($namespace)                                                      // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
              //s  ->customHeader($customHeader)                                   // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class                
               // ->cookie()                                                      // Optional: (parameters: $name,$value)
               // ->location()                                                    // Optional: (parameter: $location)
                //->certificate()                                                 // Optional: (parameter: $certLocation)
                ->cache('WSDL_CACHE_NONE')                                        // Optional: Set the WSDL cache
                ->options(['login' => 'username', 'password' => 'password']);   // Optional: Set some extra options
        });

        $data = [
            'CurrencyFrom' => 'USD',
            'CurrencyTo'   => 'EUR',
            'RateDate'     => '2014-06-05',
            'Amount'       => '1000'
        ];

        // Using the added service
        SoapWrapper::service('currency', function ($service) use ($data) {
            var_dump($service->getFunctions());
            var_dump($service->call('GetConversionAmount', [$data])->GetConversionAmountResult);
        }); 
    }
     public function indexs()
    {
                // Add a new service to the wrapper
        SoapWrapper::add(function ($service) {
            $service
                ->name('currency')
                ->wsdl('http://181.29.63.146:45420/wspuntos/service.asmx?wsdl')
                ->trace(true)                                                   // Optional: (parameter: true/false)
              //  ->header($namespace)                                                      // Optional: (parameters: $namespace,$name,$data,$mustunderstand,$actor)
              //s  ->customHeader($customHeader)                                   // Optional: (parameters: $customerHeader) Use this to add a custom SoapHeader or extended class                
               // ->cookie()                                                      // Optional: (parameters: $name,$value)
               // ->location()                                                    // Optional: (parameter: $location)
                ->certificate(true)                                                 // Optional: (parameter: $certLocation)
                ->cache('WSDL_CACHE_NONE')                                        // Optional: Set the WSDL cache
                ->options(array('UserCredentials'=>array('userName' => 'WSPuntos', 'password' => 'WSPuntos2016')));   // Optional: Set some extra options
        });
$data = [
         'user' => 'WSPuntos',
         'pass'   => 'WSPuntos2016',
        ];

        // Using the added service
        SoapWrapper::service('currency', function ($service) use ($data) {
            echo('<pre>');
            print_r($service->getFunctions());
            echo('<pre>');
            //die();
            print_r($service->call('WSPuntosObtenerDatosUsuariosAUTH', [$data])->WSPuntosObtenerDatosUsuariosAUTHResult);
        }); 
    }


    public function indexd()
    {

              SoapWrapper::add(function ($service) {
               


                   $service
                   ->name('WSPuntosObtenerDatosUsuariosAUTH')
                   ->wsdl('http://181.29.63.146:45420/wspuntos/service.asmx?wsdl')
                //   ->wsdl('http://www.holidaywebservice.com/HolidayService_v2/HolidayService2.asmx?wsdl')
                   ->cache(WSDL_CACHE_NONE) 
                   ->header(
                        'http://bpaconsultores.noip.me:45420/WSPuntos/',
                        'UserCredentials',
                        array(
                            'userName' => 'WSPuntos',
                            'password' => 'WSPuntos2016'
                        ),null)
                   ->trace(true)
                   ->certificate(true)
                   ->options(['soap_version'=> SOAP_1_2]);
                 });
            $data = ['I_vPORCIVA'=>1];
            // Using the added service
            SoapWrapper::service('WSPuntosObtenerDatosUsuariosAUTH', function ($service) use ($data) {
      echo('<pre>');
            print_r($service->getFunctions());
            echo('<pre>');

            dd($service->call('WSPuntosConsultaArticuloPuntosAUTH', [$data]));
            //var_dump($service->call('Otherfunction'));
            });

    }



    public function login(Request $request)
    {

        /*
        $user = $request->input('user');
        $pass = $request->input('pass');

        die($user);
        //
*/

         $data       =   $request->all();


        $v          =   Validator::make($data, [
            'user'     => 'required',            
            'pass'       => 'required'
        ]);


        

        if ($v->fails()) {
            // Redirect
            
          die('ee');
        }
    }
}
