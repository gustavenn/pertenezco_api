<?php

namespace App\Http\Controllers;
 
use App\WService;
use Illuminate\Http\Request;
use Validator;


class WServiceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $user = 'WSPuntos';
    private $pass = 'WSPuntos2016';
    public function __construct()
    {
        //
    }
  


    public function puntosObtenerDatosUsuariosPostParam(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'user'     => 'required',            
            'pass'       => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
       

           $xmlBody= '<?xml version="1.0" encoding="UTF-8"?>' .
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
                '<SOAP-ENV:Header>'.
                    '<ns1:UserCredentials>'.
                        '<ns1:userName>'.$data['user'].'</ns1:userName>'.
                        '<ns1:password>'.$data['pass'].'</ns1:password>'.
                    '</ns1:UserCredentials>'.
                '</SOAP-ENV:Header>'.
                '<SOAP-ENV:Body>'.
                    '<ns1:WSPuntosObtenerDatosUsuariosAUTH/>'.
                '</SOAP-ENV:Body>'.
            '</SOAP-ENV:Envelope>';

        $salidaXml= $this->_curl($xmlBody);

        $xmlReturn =$salidaXml->xpath('//DatosUsuario');

        $xmlReturn=array('result' =>$xmlReturn );

        return response()->json($xmlReturn);          
    }

     public function puntosObtenerDatosUsuarios()
    {

        $xmlBody= '<?xml version="1.0" encoding="UTF-8"?>' .
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
                '<SOAP-ENV:Header>'.
                    '<ns1:UserCredentials>'.
                        '<ns1:userName>'.$this->user.'</ns1:userName>'.
                        '<ns1:password>'.$this->pass.'</ns1:password>'.
                    '</ns1:UserCredentials>'.
                '</SOAP-ENV:Header>'.
                '<SOAP-ENV:Body>'.
                    '<ns1:WSPuntosObtenerDatosUsuariosAUTH/>'.
                '</SOAP-ENV:Body>'.
            '</SOAP-ENV:Envelope>';

        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
        
        $xmlReturn =$salidaXml->xpath('//DatosUsuario');

        $xmlReturn=array('result' =>$xmlReturn );

        return response()->json($xmlReturn);          
    }

    public function puntosConsultaArticuloPuntos(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'porciva'     => 'integer'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaArticuloPuntosAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vPORCIVA>'.$data['porciva'].'</I_vPORCIVA>'.
            '</WSPuntosConsultaArticuloPuntosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaArticuloPuntosAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }

    public function puntosConsultaDisponibles(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'idclienteusuario'     => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaDisponiblesAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_USUARIO>'.$data['idclienteusuario'].'</I_vIDCLIENTE_USUARIO>'.
            '</WSPuntosConsultaDisponiblesAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaDisponiblesAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }

     public function puntosConsultaRubrosOpcion(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'idclientecomunidad'     => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaRubrosOpcionAUTH  xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosConsultaRubrosOpcionAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaRubrosOpcionAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }

    public function puntosConsultaValorPuntos(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'idclientecomunidad'     => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaValorPuntosAUTH   xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosConsultaValorPuntosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaValorPuntosAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }

    public function puntosConsumir(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'documento'      => 'required'
            ,'idcliente'      => 'required'
            ,'puntos'         => 'required'
            ,'idproducto'     => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsumirAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
               '<I_vNRODOCUMENTO>'.$data['documento'].'</I_vNRODOCUMENTO>'.
               '<I_vIDCLIENTE_USUARIO>'.$data['idcliente'].'</I_vIDCLIENTE_USUARIO>'.
               '<I_vPUNTOS>'.$data['puntos'].'</I_vPUNTOS>'.
               '<I_vIDPRODUCTO>'.$data['idproducto'].'</I_vIDPRODUCTO>'.
            '</WSPuntosConsumirAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsumirAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }


    public function puntosDetalleExcepcion(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'nroexcepcion'     => 'integer'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosDetalleExcepcionAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<NroExcepcion>'.$data['nroexcepcion'].'</NroExcepcion>'.
            '</WSPuntosDetalleExcepcionAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosDetalleExcepcionAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }


    public function puntosObtenerRubros(Request $request)
    {


        $data       =   $request->all();
        $v          =   Validator::make($data, [
            'idclientecomunidad'     => 'required'
        ]);

        if ($v->fails()) {        
          
          $errors=array('errors' => $v->errors()->all() );
          return response()->json($errors); 

        }
    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosObtenerRubrosAUTH  xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosObtenerRubrosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');

        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return response()->json($errors);

        }

      
        $item= $salidaXml->xpath('//hob:WSPuntosObtenerRubrosAUTHResult');
       
    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return response()->json($xmlReturn);          
    }


   private function _curl ( $xmlPost)
   {
             $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_PORT => "45420",
              CURLOPT_URL => "http://181.29.63.146:45420/wspuntos/service.asmx?wsdl",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $xmlPost,
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              //echo "cURL Error #:" . $err;
                return false;
            } else {
               $xml = simplexml_load_string($response);
               $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
               $xml->registerXPathNamespace('hob', 'http://bpaconsultores.noip.me:45420/WSPuntos/');

               return $xml;


             
            }



   }
}
