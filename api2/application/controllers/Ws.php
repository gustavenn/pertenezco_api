<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ws extends CI_Controller {

	private $user = 'WSPuntos';
    private $pass = 'WSPuntos2016';

    public function __construct() {
         parent:: __construct();
      
         $this->load->helper('return_json');

    }

     public function puntosObtenerDatosUsuarios()
    {


            $xmlBody= '<?xml version="1.0" encoding="UTF-8"?>' .
            '<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
                '<SOAP-ENV:Header>'.
                    '<ns1:UserCredentials>'.
                        '<ns1:userName>'.$this->user.'</ns1:userName>'.
                        '<ns1:password>'.$this->pass.'</ns1:password>'.
                    '</ns1:UserCredentials>'.
                '</SOAP-ENV:Header>'.
                '<SOAP-ENV:Body>'.
                    '<ns1:WSPuntosObtenerDatosUsuariosAUTH/>'.
                '</SOAP-ENV:Body>'.
            '</SOAP-ENV:Envelope>';

        $salidaXml= $this->_curl($xmlBody);

        $xmlReturn =$salidaXml->xpath('//DatosUsuario');

        $xmlReturn=array('result' =>$xmlReturn );

       // return response()->json($xmlReturn);     
        return_json($xmlReturn);
        return;   
    }

     public function puntosConsultaArticuloPuntos()
    {


        $this->load->library('form_validation');
    

        $data = array(
        'porciva' => $this->input->get('porciva')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('porciva', 'porciva', 'integer');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }
     
       


         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaArticuloPuntosAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vPORCIVA>'.$data['porciva'].'</I_vPORCIVA>'.
            '</WSPuntosConsultaArticuloPuntosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                 // return response()->json($errors);
                return return_json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaArticuloPuntosAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

     
        return return_json($xmlReturn);
                 
    }


 public function puntosConsultaDisponibles()
    {


        $this->load->library('form_validation');
    
        $data = array(
        'idclienteusuario' => $this->input->get('idclienteusuario')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('idclienteusuario', 'idclienteusuario', 'required');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }

    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaDisponiblesAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_USUARIO>'.$data['idclienteusuario'].'</I_vIDCLIENTE_USUARIO>'.
            '</WSPuntosConsultaDisponiblesAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                 // return response()->json($errors);
                  return return_json($errors);
        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaDisponiblesAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return return_json($xmlReturn);          
    }

     public function puntosConsultaRubrosOpcion()
    {

  
        $this->load->library('form_validation');
    
        $data = array(
        'idclientecomunidad' => $this->input->get('idclientecomunidad')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('idclientecomunidad', 'idclientecomunidad', 'required');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }



         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaRubrosOpcionAUTH  xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosConsultaRubrosOpcionAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
            return return_json($errors);          
        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaRubrosOpcionAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return return_json($xmlReturn);          
    }

	public function puntosConsultaValorPuntos()
    {

        $this->load->library('form_validation');
    
        $data = array(
        'idclientecomunidad' => $this->input->get('idclientecomunidad')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('idclientecomunidad', 'idclientecomunidad', 'required');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }


         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsultaValorPuntosAUTH   xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosConsultaValorPuntosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);
        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                  return return_json($errors); 

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsultaValorPuntosAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return return_json($xmlReturn);           
    }

    public function puntosConsumir()
    {

      
        $this->load->library('form_validation');
    /*

documento=300964157
&idcliente=YP001
&puntos=222
&idproducto=1
    */

        $data = array(
        'documento' => $this->input->post('documento')
        ,'idcliente' => $this->input->post('idcliente')
        ,'puntos' => $this->input->post('puntos')
        ,'idproducto' => $this->input->post('idproducto')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('documento', 'documento', 'required');
        $this->form_validation->set_rules('idcliente', 'idcliente', 'required');
        $this->form_validation->set_rules('puntos', 'puntos', 'required');
        $this->form_validation->set_rules('idproducto', 'idproducto', 'required');


        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }



         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosConsumirAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
               '<I_vNRODOCUMENTO>'.$data['documento'].'</I_vNRODOCUMENTO>'.
               '<I_vIDCLIENTE_USUARIO>'.$data['idcliente'].'</I_vIDCLIENTE_USUARIO>'.
               '<I_vPUNTOS>'.$data['puntos'].'</I_vPUNTOS>'.
               '<I_vIDPRODUCTO>'.$data['idproducto'].'</I_vIDPRODUCTO>'.
            '</WSPuntosConsumirAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                 
                  return return_json($errors);           


        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosConsumirAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return return_json($xmlReturn);           
          
    }


    public function puntosDetalleExcepcion()
    {

    
    	$this->load->library('form_validation');
    
        $data = array(
        'nroexcepcion' => $this->input->get('nroexcepcion')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('nroexcepcion', 'nroexcepcion', 'integer');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }


         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosDetalleExcepcionAUTH xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<NroExcepcion>'.$data['nroexcepcion'].'</NroExcepcion>'.
            '</WSPuntosDetalleExcepcionAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');
        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
            return return_json($errors);

        }
      
        $item= $salidaXml->xpath('//hob:WSPuntosDetalleExcepcionAUTHResult');

    
        $xmlReturn=array('result' =>(string)$item[0] );   

        return return_json($xmlReturn);          
    }



    public function puntosObtenerRubros()
    {

	

    	$this->load->library('form_validation');
    
        $data = array(
        'idclientecomunidad' => $this->input->get('idclientecomunidad')
    
		);
		$this->form_validation->set_data($data);

        $this->form_validation->set_rules('idclientecomunidad', 'idclientecomunidad', 'required');

        if ($this->form_validation->run() == FALSE)
        {

      			 $errors=array('errors' => 'The fields is required.');

                  return_json($errors);
          		  return;
        }

    

         $xmlBody= '<?xml version="1.0" encoding="utf-8"?>'.
        '<soap:Envelope xmlns:xsi="http://www.w3.org/2003/05/soap-envelope" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">'.
          '<soap:Header>'.
            '<UserCredentials xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<userName>'.$this->user.'</userName>'.
              '<password>'.$this->pass.'</password>'.
            '</UserCredentials>'.
          '</soap:Header>'.
          '<soap:Body>'.
            '<WSPuntosObtenerRubrosAUTH  xmlns="http://bpaconsultores.noip.me:45420/WSPuntos/">'.
              '<I_vIDCLIENTE_COMUNIDAD>'.$data['idclientecomunidad'].'</I_vIDCLIENTE_COMUNIDAD>'.
            '</WSPuntosObtenerRubrosAUTH>'.
          '</soap:Body>'.
        '</soap:Envelope>';

  
        $salidaXml= $this->_curl($xmlBody);

        $xmlError =$salidaXml->xpath('//soap:Fault');

        if (!empty($xmlError)) {
            $errors=array('errors' => $xmlError );
                return return_json($errors);          


        }

      
        $item= $salidaXml->xpath('//hob:WSPuntosObtenerRubrosAUTHResult');
       
    
      //$xmlReturn=array('result' =>(string)$item[0] );   
                $xmlReturn=array('result' =>$item );


        return return_json($xmlReturn);          

    }



   private function _curl ( $xmlPost)
   {
             $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_PORT => "45420",
              CURLOPT_URL => "http://181.29.63.146:45420/wspuntos/service.asmx?wsdl",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => $xmlPost,
              CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: text/xml"
              ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
              //echo "cURL Error #:" . $err;
                return false;
            } else {
               $xml = simplexml_load_string($response);
               $xml->registerXPathNamespace("soap", "http://www.w3.org/2003/05/soap-envelope");
               $xml->registerXPathNamespace('hob', 'http://bpaconsultores.noip.me:45420/WSPuntos/');

               return $xml;


             
            }



   }
}
