<?php

if ( ! function_exists('return_json'))
{
	function return_json($response,$print=FALSE,$content='application/json')
	{
		if(!$print){
			$CI = & get_instance();
			$CI->output
	      	->set_content_type($content)
	       	->set_content_type('text/plain')		
	        ->set_output(json_encode( $response ));        
		}else{
			echo json_encode($response);
			exit;
		}
		
			
	}
}
?>